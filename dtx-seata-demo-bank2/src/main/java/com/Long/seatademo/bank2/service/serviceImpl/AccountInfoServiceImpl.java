package com.Long.seatademo.bank2.service.serviceImpl;


import com.Long.seatademo.bank2.dao.AccountInfoDao;
import com.Long.seatademo.bank2.entity.AccountChangeEvent;
import com.Long.seatademo.bank2.service.AccountInfoService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/20 22:51
 */
@Service
@Slf4j
public class AccountInfoServiceImpl implements AccountInfoService {
    @Resource(name = "rocketMQTemplate")
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private AccountInfoDao accountInfoDao;

    @Override
    public void addAccountInfoBanlance(AccountChangeEvent accountChangeEvent) {
        log.info("--------------------------------------------接收到消息");
        if (accountInfoDao.isExistTx(accountChangeEvent.getTxNo()) > 0) {
            return;
        }
        //增加金额
        accountInfoDao.updateAccountBalance(accountChangeEvent.getTxNo(), accountChangeEvent.getAmount());
        //添加事务号 用于幂等
        accountInfoDao.addTx(accountChangeEvent.getTxNo());

    }

    @Override
    public void doUpdateAccountBalance(AccountChangeEvent accountChangeEvent) {

    }
}







