package com.Long.seatademo.bank2.controller;

import com.Long.seatademo.bank2.service.Bank2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/14 21:55
 */
@RestController
@RequestMapping("bank")
public class Bank2Controller {

    @Autowired
    private Bank2Service bank2Service;

    @GetMapping("/money")
    public void getMoney() {
        bank2Service.getMoney();
    }


}
