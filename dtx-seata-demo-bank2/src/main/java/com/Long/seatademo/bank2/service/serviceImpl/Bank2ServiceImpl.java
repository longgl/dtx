package com.Long.seatademo.bank2.service.serviceImpl;

import com.Long.seatademo.bank2.dao.AccountInfoDao;
import com.Long.seatademo.bank2.service.Bank2Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/14 21:56
 */
@Service
@Slf4j
public class Bank2ServiceImpl implements Bank2Service {

    @Autowired
    private AccountInfoDao accountInfoDao;

    @Transactional
    @Override
    public void getMoney() {
        Double d=1000.0;
        log.info("begin update banlance!");
        accountInfoDao.updateAccountBalance("2",d);
        log.info("end update banlance!");
    }
}
