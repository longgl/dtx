package com.Long.seatademo.bank2.message;

import com.Long.seatademo.bank2.entity.AccountChangeEvent;
import com.Long.seatademo.bank2.service.AccountInfoService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/24 15:38
 */
@Component
@Slf4j
@RocketMQMessageListener(consumerGroup = "consumer_group_txms_bank1", topic = "topic_txMms")
public class TxmsgConsumer implements RocketMQListener<String> {

    @Autowired
    private AccountInfoService accountInfoService;

    //接收消息
    @Override
    public void onMessage(String message) {
        log.info("开始消费消息------>{}", message);
        AccountChangeEvent accountChangeEvent = (AccountChangeEvent) JSONObject.parseObject(message).get("accountChange");
        //
        accountChangeEvent.setAccountNo("2");
        //更新本地账户，增加金额
        accountInfoService.addAccountInfoBanlance(accountChangeEvent);
        log.info("消费消息结束!");


    }
}
