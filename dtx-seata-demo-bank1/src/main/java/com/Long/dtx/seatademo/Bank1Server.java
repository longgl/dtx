
package com.Long.dtx.seatademo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.Long.dtx.seatademo.bank1.feign")
public class Bank1Server {

	public static void main(String[] args) {
		SpringApplication.run(Bank1Server.class, args);

	}

}
