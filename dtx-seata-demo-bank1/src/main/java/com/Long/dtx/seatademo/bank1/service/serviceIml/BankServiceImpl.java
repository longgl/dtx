package com.Long.dtx.seatademo.bank1.service.serviceIml;

import com.Long.dtx.seatademo.bank1.dao.AccountInfoDao;
import com.Long.dtx.seatademo.bank1.feign.Bank2FeignService;
import com.Long.dtx.seatademo.bank1.service.BanKService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/12 22:14
 */
@Service
@Slf4j
public class BankServiceImpl implements BanKService {

    @Autowired
    private AccountInfoDao accountInfoDao;
    @Autowired
    private Bank2FeignService bank2FeignService;




    @Transactional
    @Override
    public void getMoney() {
        Double d = 1000.0;
        log.info("调用Bank2服务开始");
        bank2FeignService.getMoney();
        log.info("调用Bank2服务结束");
        int a = 1 / 0;
        accountInfoDao.updateAccountBalance("1", d);
    }
}
