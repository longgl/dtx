package com.Long.dtx.seatademo.bank1.entity;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/20 22:53
 */
public class AccountChangeEvent {
    private String accountNo;
    private double amount;
    private String txNo;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTxNo() {
        return txNo;
    }

    public void setTxNo(String txNo) {
        this.txNo = txNo;
    }
}
