package com.Long.dtx.seatademo.bank1.service;

import com.Long.dtx.seatademo.bank1.entity.AccountChangeEvent;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/20 22:51
 */
public interface AccountInfoService {
    public void sendUpdateAccountBalance(AccountChangeEvent accountChangeEvent);

    public void doUpdateAccountBalance(AccountChangeEvent accountChangeEvent);


}
