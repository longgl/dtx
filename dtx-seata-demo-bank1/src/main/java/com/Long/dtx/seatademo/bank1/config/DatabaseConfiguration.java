package com.Long.dtx.seatademo.bank1.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.hibernate.HikariConnectionProvider;
import io.netty.util.internal.StringUtil;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;


/*@Configuration*/
public class DatabaseConfiguration {

    /*@Autowired
    private DataSourceProperties dataSourceProperties;

    @Bean
    public DataSource DatabaseConfiguration(DataSourceProperties dataSourceProperties) {
        *//*
         * 可以看出Spring Boot 2.0.4 默认使用 com.zaxxer.hikari.HikariDataSource 数据源，
         * 而以前版本，如 Spring Boot 1.5 默认使用
         *  org.apache.tomcat.jdbc.pool.DataSource 作为数据源；
         * *//*
        HikariDataSource dataSource = dataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
        if (StringUtils.hasText(dataSourceProperties.getName())) {
            dataSource.setPoolName(dataSourceProperties.getName());
        }
        return new DataSourceProxy(dataSource);
    }
*/

}
