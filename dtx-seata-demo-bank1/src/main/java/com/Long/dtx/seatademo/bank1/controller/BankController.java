package com.Long.dtx.seatademo.bank1.controller;

import com.Long.dtx.seatademo.bank1.entity.AccountChangeEvent;
import com.Long.dtx.seatademo.bank1.entity.AccountInfo;
import com.Long.dtx.seatademo.bank1.service.AccountInfoService;
import com.Long.dtx.seatademo.bank1.service.BanKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/12 22:09
 */
@RestController
@RequestMapping("bank")
public class BankController {

    @Autowired
    private BanKService banKService;
    @Autowired
    private AccountInfoService accountInfoService;

    @GetMapping("/money")
    public void getMoney() {
        banKService.getMoney();
    }

    @GetMapping("/money2")
    public void getMoneyTX() {
        AccountChangeEvent accountChangeEvent=new AccountChangeEvent();
        accountChangeEvent.setAccountNo("1");
        accountChangeEvent.setAmount(1000.0);
        accountInfoService.sendUpdateAccountBalance(accountChangeEvent);
    }

}
