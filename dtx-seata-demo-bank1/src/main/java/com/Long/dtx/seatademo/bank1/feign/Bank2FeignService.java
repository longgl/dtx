package com.Long.dtx.seatademo.bank1.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author GL
 * @version 1.0
 * @date 2021/4/14 22:23
 */
@FeignClient("dtx-seata-demo-bank2")
public interface Bank2FeignService {

    @RequestMapping("/bank2/bank/money")
    public void getMoney();
}
